#pragma once

// CReportViewerDoc document

class CReportViewerDoc : public CDocument
{
	DECLARE_DYNCREATE(CReportViewerDoc)

public:
	CReportViewerDoc();
	virtual ~CReportViewerDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual BOOL OnNewDocument();

	DECLARE_MESSAGE_MAP()
};
