#pragma once

#define __BUILD
#ifdef __BUILD
#define DLL_BUILD __declspec(dllexport)
#else
#define DLL_BUILD __declspec(dllimport)
#endif

// Initialize the DLL, register the classes etc
extern "C" AFX_EXT_API void InitSuite(CStringArray *, vecINDEX_TABLE &, vecINFO_TABLE &);

// Open a document view
extern "C" AFX_EXT_API void OpenSuite(int idx, LPCTSTR func, CWnd *, vecINDEX_TABLE &, int *ret);
extern "C" AFX_EXT_API void OpenSuiteEx(_user_msg *msg, CWnd *, vecINDEX_TABLE &, int *ret);


#using <mscorlib.dll>
#include <afxwinforms.h>   // MFC Windows Forms support
