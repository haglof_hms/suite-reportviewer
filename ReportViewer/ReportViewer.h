#pragma once

#ifndef _ReportViewer_H_
#define _ReportViewer_H_

#include <vector>

typedef std::vector<INDEX_TABLE> vecINDEX_TABLE;
typedef std::vector<INFO_TABLE> vecINFO_TABLE;

extern RLFReader* g_pXML;
extern HINSTANCE hInst;
extern CWinApp* pApp;


#define __BUILD
#ifdef __BUILD
#define DLL_BUILD __declspec(dllexport)
#else
#define DLL_BUILD __declspec(dllimport)
#endif

class CReportViewer
{
public:
	CReportViewer();
	~CReportViewer();
};

extern CReportViewer theApp;


#endif