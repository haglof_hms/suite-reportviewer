// ReportViewerView.h : interface of the CReportViewerView class
//

#pragma once

#include <afxwinforms.h>
#include "ReportViewerDoc.h"
#include "resource.h"

#ifdef MYDEBUG
  #using <../RDL/bin/Debug/RDL.dll>
#else
  #using <../RDL/bin/Release/RDL.dll>
#endif

using namespace Microsoft::Reporting::WinForms;
using namespace System::Windows::Forms;


class CReportViewerView : public CFormView
{
public:
	CReportViewerView();

protected: // create from serialization only
	DECLARE_DYNCREATE(CReportViewerView)

	Microsoft::VisualC::MFC::CWinFormsControl<ReportViewer> m_reportWnd;

// Attributes
public:
	CReportViewerDoc* GetDocument() const;

protected:
// Implementation
	CString getConnectionString();
	void showParamsPrompt();

public:
	enum { IDD = IDD_FORMVIEW };

	void showReport(LPCTSTR filename, LPCTSTR args = NULL);

	virtual void OnInitialUpdate(); // called first time after construct
	virtual ~CReportViewerView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	BEGIN_DELEGATE_MAP(CReportViewerView)
		EVENT_DELEGATE_ENTRY(OnZoomChange, System::Object^, ZoomChangeEventArgs^)
	END_DELEGATE_MAP()

	afx_msg void OnStep(int nID);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	void button_Click(System::Object^ sender, System::EventArgs^ e);

	// Generated message map functions
	afx_msg void OnSetStatusbar(int nCurrent);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	DECLARE_MESSAGE_MAP()

	void OnZoomChange(System::Object^ sender, ZoomChangeEventArgs^ e);
};

#ifndef _DEBUG  // debug version in ReportViewerView.cpp
inline CReportViewerDoc* CReportViewerView::GetDocument() const
   { return reinterpret_cast<CReportViewerDoc*>(m_pDocument); }
#endif
