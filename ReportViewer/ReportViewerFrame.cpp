#include "stdafx.h"
#include "ReportViewerFrame.h"
#include "ReportViewerView.h"
#include "ReportViewer.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CReportViewerFrame

IMPLEMENT_DYNCREATE(CReportViewerFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CReportViewerFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CReportViewerFrame)
	//}}AFX_MSG_MAP
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_MESSAGE(XTPWM_DOCKINGPANE_NOTIFY, OnDockingPaneNotify)
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMsgSuite)
	ON_WM_SYSCOMMAND()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CReportViewerFrame construction/destruction

CReportViewerFrame::CReportViewerFrame()
{
	m_bOnce = TRUE;
}

CReportViewerFrame::~CReportViewerFrame()
{
}

BOOL CReportViewerFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CReportViewerFrame::OnSysCommand(UINT nID,LPARAM lParam)
{
	if ((nID & 0xFFF0) == SC_CLOSE) // && m_bOkToClose)
		CMDIChildWnd::OnSysCommand(nID,lParam);
	
	if ((nID & 0xFFF0) == SC_MAXIMIZE || 
			(nID & 0xFFF0) == SC_MINIMIZE || 
			(nID & 0xFFF0) == SC_MOVE || 
			(nID & 0xFFF0) == SC_RESTORE || 
			(nID & 0xFFF0) == SC_SIZE)
		CMDIChildWnd::OnSysCommand(nID,lParam);
}

/////////////////////////////////////////////////////////////////////////////
// CReportViewerFrame diagnostics

#ifdef _DEBUG
void CReportViewerFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CReportViewerFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CReportViewerFrame message handlers

void CReportViewerFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
	{
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
	}
}

LRESULT CReportViewerFrame::OnDockingPaneNotify(WPARAM wParam, LPARAM lParam)
{
	if (wParam == XTP_DPN_SHOWWINDOW)
	{
		return TRUE;
	}
	return FALSE;
}

void CReportViewerFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

    if(bShow && !IsWindowVisible() && m_bOnce == TRUE)
    {
		m_bOnce = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\ReportViewer\\Dialogs\\Report"), REG_ROOT);
		LoadPlacement(this, csBuf);
    }
}

LRESULT CReportViewerFrame::OnMsgSuite(WPARAM wParm, LPARAM lParm)
{
	// user pushed some buttons in the shell.
	switch(wParm)
	{
		case ID_DBNAVIG_START:
			((CReportViewerView*)GetActiveView())->OnStep(wParm);
		break;

		case ID_DBNAVIG_NEXT:
			((CReportViewerView*)GetActiveView())->OnStep(wParm);
		break;

		case ID_DBNAVIG_PREV:
			((CReportViewerView*)GetActiveView())->OnStep(wParm);
		break;

		case ID_DBNAVIG_END:
			((CReportViewerView*)GetActiveView())->OnStep(wParm);
		break;
	};

	return 0;
}

void CReportViewerFrame::OnDestroy()
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\ReportViewer\\Dialogs\\Report"), REG_ROOT);
	SavePlacement(this, csBuf);
	m_bOnce = TRUE;

	CXTPFrameWndBase<CMDIChildWnd>::OnDestroy();
}

void CReportViewerFrame::OnClose()
{
	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);

	CXTPFrameWndBase<CMDIChildWnd>::OnClose();
}
