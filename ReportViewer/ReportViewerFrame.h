// ReportViewerFrame.h : interface of the CReportViewerFrame class
//

#pragma once

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>

class CReportViewerFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CReportViewerFrame)

	BOOL m_bOnce;

public:
	CReportViewerFrame();

// Attributes
public:
	CXTPDockingPaneManager m_paneManager;
	CXTPPropertyGrid m_wndPropertyGrid;

// Operations
public:
	static XTPDockingPanePaintTheme m_themeCurrent;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CReportViewerFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CReportViewerFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	LRESULT OnMsgSuite(WPARAM wParm, LPARAM lParm);

// Generated message map functions
protected:
	//{{AFX_MSG(CReportViewerFrame)
	afx_msg LRESULT OnDockingPaneNotify(WPARAM wParam, LPARAM lParam);
	afx_msg	void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg void OnSysCommand(UINT nID,LPARAM lParam);
	afx_msg void OnZoomChange();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
};
