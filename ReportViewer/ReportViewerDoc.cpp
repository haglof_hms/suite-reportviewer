// ReportViewerDoc.cpp : implementation file
//

#include "stdafx.h"
#include "ReportViewer.h"
#include "ReportViewerDoc.h"


// CReportViewerDoc

IMPLEMENT_DYNCREATE(CReportViewerDoc, CDocument)

CReportViewerDoc::CReportViewerDoc()
{
}

BOOL CReportViewerDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	return TRUE;
}

CReportViewerDoc::~CReportViewerDoc()
{
}


BEGIN_MESSAGE_MAP(CReportViewerDoc, CDocument)
END_MESSAGE_MAP()


// CReportViewerDoc diagnostics

#ifdef _DEBUG
void CReportViewerDoc::AssertValid() const
{
	CDocument::AssertValid();
}

#ifndef _WIN32_WCE
void CReportViewerDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif
#endif //_DEBUG

// CReportViewerDoc commands
