// ReportViewerView.cpp : implementation of the CReportViewerView class
//

#include "stdafx.h"
#include "ReportViewer.h"
#include "ReportViewerDoc.h"
#include "ReportViewerView.h"
#include "ReportViewerFrame.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// Class that defines event handler for managed form
ref class EventReceiver
{
public:
   void OnClick(System::Object^ sender, System::EventArgs^ e)
   {
	   System::Windows::Forms::MessageBox::Show(gcnew System::String("click"));

	   System::Collections::Generic::KeyValuePair<Control^, ListBox^>^ kv = (System::Collections::Generic::KeyValuePair<Control^, ListBox^>^)(((Button^)sender)->Tag);
	   if( kv->Key->GetType() == System::Windows::Forms::TextBox::typeid )
	   {
		   TextBox ^txt = (TextBox^)kv->Key;
		   ListBox ^lst = (ListBox^)kv->Value;
		   ((System::ComponentModel::BindingList<System::String^>^)lst->DataSource)->Add(txt->Text);
		   txt->Clear();
	   }
   }
};


// CReportViewerView
IMPLEMENT_DYNCREATE(CReportViewerView, CFormView)

BEGIN_MESSAGE_MAP(CReportViewerView, CFormView)
	//{{AFX_MSG_MAP(CReportViewerView)
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//COleControlSite

// CReportViewerView construction/destruction
CReportViewerView::CReportViewerView()
	: CFormView(CReportViewerView::IDD)
{
}

CReportViewerView::~CReportViewerView()
{
}

void CReportViewerView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);

	Microsoft::VisualC::MFC::DDX_ManagedControl(pDX, IDC_STATIC, m_reportWnd);
}

// CReportViewerView diagnostics

#ifdef _DEBUG
void CReportViewerView::AssertValid() const
{
	CFormView::AssertValid();
}

void CReportViewerView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CReportViewerDoc* CReportViewerView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CReportViewerDoc)));
	return (CReportViewerDoc*)m_pDocument;
}
#endif //_DEBUG

CString CReportViewerView::getConnectionString()
{
	TCHAR sDB_PATH[127], sUserName[127], sPSW[127], sDSN[127], sLocation[127], sDBName[127];
	SAClient_t m_saClient;
	int nServerConn=0;
	CString sConnStr;

	if(getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
	GetAuthentication(&nServerConn);
	if(nServerConn)
	{
		sConnStr.Format(_T("Provider=SQLOLEDB;OLE DB Services=-4;Server=%s;Database=%s;Uid=%s;Pwd=%s"), sLocation, sDBName, sUserName, sPSW);
	}
	else
	{
		sConnStr.Format(_T("Provider=SQLOLEDB;OLE DB Services=-4;Server=%s;Database=%s;Trusted_Connection=True"), sLocation, sDBName);
	}

	return sConnStr;
}

void CReportViewerView::showReport(LPCTSTR filename, LPCTSTR args /*= NULL*/)
{
	LocalReport^ report = m_reportWnd.GetControl()->LocalReport;
	System::String^ sFilename = gcnew System::String(filename);

	// Open report
	report->ReportPath = sFilename;
	report->DataSources->Clear();

	// Parse RDL. Run each query and attach dataset to report
	RDL::Report^ reportDef = RDL::Report::GetReportFromFile(sFilename);
	for each (RDL::DataSet^ ds in reportDef->DataSets)
	{
		//copy the parameters from the QueryString into the ReportParameters definitions (objects)
		//ds->AssignParameters(this.ReportParameters);

		System::Data::DataTable^ tbl = ds->GetDataTable(gcnew System::String(getConnectionString()));
		ReportDataSource^ rds = gcnew ReportDataSource();
		rds->Name = ds->Name;
		rds->Value = tbl;
		report->DataSources->Add(rds);
	}

	showParamsPrompt();

	//Load any other report parameters (which are not part of the DB query).  
	//If any of the parameters are required, make sure they were provided, or show an error message.  Note: SSRS cannot render the report if required parameters are missing
	//CheckReportParameters(rvReportViewer.LocalReport);

	// Show report
	m_reportWnd.GetControl()->RefreshReport();


	// TODO: handle arguments
	if(args) AfxMessageBox(args);
}

void CReportViewerView::showParamsPrompt()
{
	LocalReport^ report = m_reportWnd.GetControl()->LocalReport;
	Form^ dlg = gcnew Form();
	Control^ ctrl;
	Button^ btn;
	Label^ lbl;
	ListBox^ lst;
	ErrorProvider^ err = gcnew ErrorProvider();
	int index = 0;

	err->ContainerControl = dlg;

    ReportParameterInfoCollection^ params = report->GetParameters();
    for each(ReportParameterInfo^ param in params)
	{
		// Create control
		switch(param->DataType)
		{
		case ParameterDataType::Boolean:
		case ParameterDataType::DateTime:
		case ParameterDataType::Float:
		case ParameterDataType::Integer:
		case ParameterDataType::String:
			ctrl = (Control^)gcnew TextBox();
			break;
		}

		// Add control to form
		ctrl->Left = 20;
		ctrl->Top = index * 120 + 20;
		dlg->Controls->Add(ctrl);

		// Add label
		lbl = gcnew Label();
		lbl->Left = 20;
		lbl->Top = index * 120;
		lbl->Text = param->Prompt;
		dlg->Controls->Add(lbl);

		// Add list of selected values for multi value
		if(param->MultiValue)
		{
			lst = gcnew ListBox();
			lst->Left = 160;
			lst->Top = index * 120 + 20;
			dlg->Controls->Add(lst);

			btn = gcnew Button();
			btn->Left = 120;
			btn->Top = index * 120 + 20;
			btn->Width = 20;
			btn->Text = gcnew System::String(">");
			btn->Tag = gcnew System::Collections::Generic::KeyValuePair<Control^, ListBox^>(ctrl, lst);
			dlg->Controls->Add(btn);

			// Bind event handler
			EventReceiver^ er = gcnew EventReceiver();
			btn->Click += gcnew System::EventHandler(er, &EventReceiver::OnClick);

			// Bind to collection
			lst->DataSource = gcnew System::ComponentModel::BindingList<System::String^>();

			// Test
			//err->SetError(ctrl, "test");
		}

		index++;

		// Add event validators
	}

	/*
	Allow blank value (text only)		(TextValidator)
	Allow null value					(NonNullValidator)
	Allow multiple values (except bool)	(add/remove from listbox)

	(ComboBox: Use Data Bound Items)
	Available values (static or from dataset)
	Default values (static or from dataset)

	Cascading parameters
	Determine dependencies (Parameter->QueryParameter->DataSet->Parameter)
	Refresh when changed
	*/

	dlg->AutoScroll = true;
	dlg->FormBorderStyle = FormBorderStyle::FixedDialog;
	dlg->MaximizeBox = false;
	dlg->StartPosition = FormStartPosition::CenterParent;
	dlg->Size = System::Drawing::Size(800, 600);

	dlg->ShowDialog();
}

void CReportViewerView::OnDestroy()
{
	CFormView::OnDestroy();
}

void CReportViewerView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	if( m_reportWnd.GetControl() )
	{
		m_reportWnd.GetControl()->Width = cx + 1;
		m_reportWnd.GetControl()->Height = cy + 1;
	}
}

void CReportViewerView::OnSetFocus(CWnd* pOldWnd)
{
	CFormView::OnSetFocus(pOldWnd);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_LIST, FALSE);
}

void CReportViewerView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	m_reportWnd.GetControl()->Left = 0;
	m_reportWnd.GetControl()->Top = 0;

	m_reportWnd.GetControl()->ZoomChange	+= MAKE_DELEGATE(ZoomChangedEventHandler, OnZoomChange);
}


// CReportViewerView message handlers
void CReportViewerView::OnStep(int nID)
{
}

void CReportViewerView::OnZoomChange(System::Object^ sender, ZoomChangeEventArgs^ e)
{
	AfxMessageBox(_T("ZoomChange"));
}
