#include "stdafx.h"
#include "Resource.h"
#include "Exports.h"
#include "ReportViewer.h"
#include "ReportViewerDoc.h"
#include "ReportViewerFrame.h"
#include "ReportViewerView.h"

std::vector<HINSTANCE> m_vecHInstTable;
static AFX_EXTENSION_MODULE ReportViewerDLL = { NULL, NULL };


// Exported DLL initialization is run in context of running application
void DLL_BUILD InitSuite(CStringArray *user_modules,vecINDEX_TABLE &vecIndex,vecINFO_TABLE &vecInfo)
{
	// create a new CDynLinkLibrary for this app
	new CDynLinkLibrary(ReportViewerDLL);

	CString csModuleFN = getModuleFN(hInst);
	m_vecHInstTable.clear();

	// Setup the language filename
	CString csLangFN;
	csLangFN.Format(_T("%s%s"), getLanguageDir(), PROGRAM_NAME);

	CWinApp* pApp = AfxGetApp();


	// MS Report Viewer
	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW, 
			RUNTIME_CLASS(CReportViewerDoc),
			RUNTIME_CLASS(CReportViewerFrame),
			RUNTIME_CLASS(CReportViewerView)));
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW, csModuleFN, csLangFN, FALSE));

	// Get version information; 060803 p�d
	CString csVersion, csCopyright, csCompany;
	csVersion	= getVersionInfo(hInst, VER_NUMBER);
	csCopyright	= getVersionInfo(hInst, VER_COPYRIGHT);
	csCompany	= getVersionInfo(hInst, VER_COMPANY);

	vecInfo.push_back(INFO_TABLE(-999,
		1, //Set to 1 to indicate a SUITE; 2 indicates a  User Module,
		csLangFN,
		csVersion,
		csCopyright,
		csCompany));

	/* *****************************************************************************
		Load user module(s), specified in the ShellTree data file for this SUITE
	****************************************************************************** */
	typedef CRuntimeClass *(*Func)(CWinApp *, LPCTSTR suite, vecINDEX_TABLE &, vecINFO_TABLE &);
	Func proc;
	// Try to get modules connected to this Suite; 051129 p�d
	if (user_modules->GetCount() > 0)
	{
		for (int i = 0;i < user_modules->GetCount();i++)
		{
			CString sPath;
			sPath.Format(_T("%s%s"), getModulesDir(), user_modules->GetAt(i));
			// Check if the file exists, if not tell USER; 051213 p�d
			if (fileExists(sPath))
			{
				HINSTANCE hInst = AfxLoadLibrary(sPath);
				if (hInst != NULL)
				{
					m_vecHInstTable.push_back(hInst);
					proc = (Func)GetProcAddress((HMODULE)m_vecHInstTable[m_vecHInstTable.size() - 1], "InitModule" );
					if (proc != NULL)
					{
						// call the function
						proc(pApp, csModuleFN, vecIndex, vecInfo);
					}	// if (proc != NULL)
				}	// if (hInst != NULL)

			}	// if (fileExists(sPath))
			else
			{
				// Set Messages from language file; 051213 p�d
				::MessageBox(0,_T("File doesn't exist\n" + sPath),_T("Error"),MB_OK);
			}
		}	// for (int i = 0;i < m_sarrModules.GetCount();i++)
	}	// if (m_sarrModules.GetCount() > 0)
}

void DLL_BUILD OpenSuite(int idx, LPCTSTR func, CWnd *wnd, vecINDEX_TABLE &vecIndex, int *ret)
{
	CDocTemplate *pTemplate;
	CString sDocName;
	CString sResStr;
	CString sModuleFN;
	CString sVecIndexTableModuleFN;
	CString sLangFN;
	CString sCaption;
	int nTableIndex;
	BOOL bFound = FALSE, bIsOneInst = FALSE;
	CString sDocTitle;

	ASSERT(pApp != NULL);

	// Get path and filename of this SUITE; 051213 p�d
	sModuleFN = getModuleFN(hInst);

	// Find template name for idx value; 051124 p�d
	if (vecIndex.size() == 0)
		return;

	for (UINT i = 0;i < vecIndex.size();i++)
	{
		// Get index of this Window, as set in Doc template
		nTableIndex = vecIndex[i].nTableIndex;
		// Get filename including searchpath to THIS SUITE, as set in
		// the table index vector, for suites and module(s); 051213 p�d
		sVecIndexTableModuleFN = vecIndex[i].szSuite;

		// Get the stringtable resource, matching the TableIndex
		// This string is compared to the title of the document; 051212 p�d
		sResStr.LoadString(nTableIndex);

		bIsOneInst = vecIndex[i].bOneInstance;

		if (nTableIndex == idx && sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
		{
			// Get language filename
			sLangFN = vecIndex[i].szLanguageFN;
			bFound = TRUE;

			if (g_pXML->Load(sLangFN))
			{
				sCaption = g_pXML->str(nTableIndex);
			}
			else
			{
				AfxMessageBox(_T("Could not open languagefile!"), MB_ICONERROR);
			}

			// Check if the document or module is in this SUITE; 051213 p�d
			if (sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
			{
				POSITION pos = pApp->GetFirstDocTemplatePosition();
				while(pos != NULL)
				{
					pTemplate = pApp->GetNextDocTemplate(pos);
					pTemplate->GetDocString(sDocName, CDocTemplate::docName);
					ASSERT(pTemplate != NULL);
					// Need to add a linefeed, infront of the docName.
					// This is because, for some reason, the document title,
					// set in resource, must have a linefeed.
					// OBS! Se documentation for CMultiDocTemplate; 051212 p�d
					sDocName = '\n' + sDocName;

					if (pTemplate && sDocName.Compare(sResStr) == 0)
					{
						if(bIsOneInst == TRUE)
						{
							// Find the CDocument for this tamplate, and set title.
							// Title is set in Languagefile; OBS! The nTableIndex
							// matches the string id in the languagefile; 051129 p�d
							POSITION posDOC = pTemplate->GetFirstDocPosition();
							while(posDOC != NULL)
							{
								CReportViewerDoc* pDocument = (CReportViewerDoc*)pTemplate->GetNextDoc(posDOC);
								POSITION pos = pDocument->GetFirstViewPosition();
								if(pos != NULL)
								{
									CView* pView = pDocument->GetNextView(pos);
									pView->GetParent()->BringWindowToTop();
									pView->GetParent()->SetFocus();
									posDOC = (POSITION)1;
									break;
								}
							}

							// no previous document open
							if(posDOC == NULL)
							{
								CDocument* pDocument;
								CFrameWnd * pFrame;
								CReportViewerView *pReport;

								pDocument = (CDocument*)pTemplate->CreateNewDocument(); //OpenDocumentFile(NULL);
								if(pDocument == NULL) return;
								pDocument->OnNewDocument();

								pFrame = pTemplate->CreateNewFrame(pDocument, NULL);
								if(pFrame == NULL) return;
								pTemplate->InitialUpdateFrame(pFrame, pDocument);


								POSITION pos = pDocument->GetFirstViewPosition();
								pReport = (CReportViewerView *)pFrame->GetActiveView(); //pDocument->GetNextView(pos);


								if(pFrame == NULL || pFrame->m_hWnd == INVALID_HANDLE_VALUE || pDocument == NULL || pReport == NULL) return;

								CString sLangSet;
								sLangSet = getLangSet();
								CString sFuncStr;
								sFuncStr.Format(_T("%s%s\\%s"),getReportsDir(),sLangSet,func);

								if (!fileExists(sFuncStr))
								{
									AfxMessageBox(g_pXML->str(105));
									*ret = 1;
									return;
								}

								CWaitCursor wc;
								pReport->showReport(sFuncStr);

								sDocTitle.Format(_T("%s - [%s]"), sCaption, func);
								pDocument->SetTitle(sDocTitle);

								wc.Restore();

								break;
							}
						}
						else	//if(bIsOneInst == TRUE)
						{
							CDocument* pDocument = (CDocument*)pTemplate->CreateNewDocument(); //OpenDocumentFile(NULL);
							if(pDocument == NULL) return;
							pDocument->OnNewDocument();

							CFrameWnd * pFrame = pTemplate->CreateNewFrame(pDocument, NULL);
							if(pFrame == NULL) return;
							pTemplate->InitialUpdateFrame(pFrame, pDocument);


							POSITION pos = pDocument->GetFirstViewPosition();
							CReportViewerView *pReport = (CReportViewerView *)pFrame->GetActiveView(); //pDocument->GetNextView(pos);


							if(pFrame == NULL || pFrame->m_hWnd == INVALID_HANDLE_VALUE || pDocument == NULL || pReport == NULL) return;

							CString sLangSet;
							sLangSet = getLangSet();
							CString sFuncStr;
							sFuncStr.Format(_T("%s%s\\%s"),getReportsDir(),sLangSet,func);

							if (!fileExists(sFuncStr))
							{
								AfxMessageBox(g_pXML->str(105));
								pDocument->SetTitle(sCaption);
								*ret = 1;
								return;
							}

							CWaitCursor wc;
							pReport->showReport(sFuncStr);

							sDocTitle.Format(_T("%s - [%s]"), sCaption, func);
							pDocument->SetTitle(sDocTitle);

							wc.Restore();

							break;
						}

						break;
					}	// if (pTemplate && sDocName.Compare(sResStr) == 0)
				}	// while(pos != NULL)
			} // if (sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
			*ret = 1;
		}	// if (nTableIndex == idx)
	}	// for (UINT i = 0;i < vecIndex.size();i++)
}

CReportViewer::CReportViewer()
{
}

CReportViewer::~CReportViewer()
{
}

void runOpenSuiteEx(_user_msg *msg, CWnd *wnd, vecINDEX_TABLE &vecIndex, int *ret, enumAction action)
{
	CDocTemplate *pTemplate;
	CString sFuncStr;
	CString sDocName;
	CString sResStr;
	CString sModuleFN;
	CString sVecIndexTableModuleFN;
	CString sLangFN;
	CString sCaption;
	CString sLangSet;
	CString sFileToOpen;
	CString sCompareCaption;
	int nDocCounter;
	int nNumOfReportsOpen = 0;
	int nTableIndex;
	BOOL bFound = FALSE;
	BOOL bIsOneInst;

	ASSERT(pApp != NULL);

	if (!fileExists(msg->getName()))
	{
		AfxMessageBox(g_pXML->str(105));
		*ret = 0;
		return;
	}


	// Get path and filename of this SUITE; 051213 p�d
	sModuleFN = getModuleFN(hInst);

	// Find template name for idx value; 051124 p�d
	if (vecIndex.size() == 0)
		return;

	// Get language abbrevatein set, from registry; 060111 p�d
	sLangSet = getLangSet();

	for (UINT i = 0;i < vecIndex.size();i++)
	{
		nTableIndex = vecIndex[i].nTableIndex;
		if (nTableIndex == msg->getIndex())
		{
			// Get filename including searchpath to THIS SUITE, as set in
			// the table index vector, for suites and module(s); 051213 p�d
			sVecIndexTableModuleFN = vecIndex[i].szSuite;
			// Need to setup the Actual filename here, because we need to 
			// get the Language set in registry, on Openning a View; 051214 p�d
			sLangFN = vecIndex[i].szLanguageFN;
			sFileToOpen = msg->getFileName();

			bFound = TRUE;
			bIsOneInst = vecIndex[i].bOneInstance;
			break;
		}	// if (nTableIndex == idx)
	}	// for (UINT i = 0;i < vecIndex.size();i++)
	
	if (bFound)
	{
		// Get the stringtable resource, matching the TableIndex
		// This string is compared to the title of the document; 051212 p�d
		sResStr.LoadString(nTableIndex);

		if (g_pXML->Load(sLangFN))
		{
			sCaption = g_pXML->str(nTableIndex);
		}
		else
		{
			AfxMessageBox(_T("Could not open languagefile!"), MB_ICONERROR);
		}

		// Check if the document or module is in this SUITE; 051213 p�d
		if (sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
		{

			// First; find out number of Report wiondows already open; 060627 p�d
			POSITION posA = pApp->GetFirstDocTemplatePosition();
			nNumOfReportsOpen = 0;
			while(posA != NULL)
			{
				pTemplate = pApp->GetNextDocTemplate(posA);
				pTemplate->GetDocString(sDocName, CDocTemplate::docName);
				ASSERT(pTemplate != NULL);
				// Need to add a linefeed, infront of the docName.
				// This is because, for some reason, the document title,
				// set in resource, must have a linefeed.
				// OBS! Se documentation for CMultiDocTemplate; 051212 p�d
				sDocName = '\n' + sDocName;

				if (pTemplate && sDocName.Compare(sResStr) == 0)
				{
					POSITION posDOCA = pTemplate->GetFirstDocPosition();
					nDocCounter = 1;

					while (posDOCA != NULL)
					{
						CDocument* pDocumentA = pTemplate->GetNextDoc(posDOCA);
						sCompareCaption = pDocumentA->GetTitle();
						// Compare title of window and count number of matches for
						// a Report window; 060627 p�d
						if (_tcsnccmp(sCompareCaption, sCaption, _tcslen(sCaption)) == 0)
						{
							nNumOfReportsOpen++;
						}	// if (_tcsnccmp(sCompareCaption.GetBuffer(),
					}	// while (posDOCA != NULL)
				}	// if (pTemplate && sDocName.Compare(sResStr) == 0)
			}	// while(pos != NULL)

			POSITION pos = pApp->GetFirstDocTemplatePosition();
			while(pos != NULL)
			{
				pTemplate = pApp->GetNextDocTemplate(pos);
				pTemplate->GetDocString(sDocName, CDocTemplate::docName);
				ASSERT(pTemplate != NULL);
				// Need to add a linefeed, infront of the docName.
				// This is because, for some reason, the document title,
				// set in resource, must have a linefeed.
				// OBS! Se documentation for CMultiDocTemplate; 051212 p�d
				sDocName = '\n' + sDocName;

				if (pTemplate && sDocName.Compare(sResStr) == 0)
				{
					pTemplate->OpenDocumentFile(NULL);

					// Find the CDocument for this template, and set title.
					// Title is set in Languagefile; OBS! The nTableIndex
					// matches the string id in the languagefile; 051129 p�d
					POSITION posDOC = pTemplate->GetFirstDocPosition();
					while (posDOC != NULL)
					{
						CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
						if (nDocCounter == (nNumOfReportsOpen + 1) || nNumOfReportsOpen == 0)
						{

							POSITION posView = pDocument->GetFirstViewPosition();
							if (posView)
							{

								CReportViewerView *pReport = (CReportViewerView *)pDocument->GetNextView(posView);
								if (pReport)
								{
									CWaitCursor wc;
									pReport->showReport(msg->getName(),((CString*)msg->getVoidBuf())->GetBuffer());
									
									// set the document title to be the name of the report
									CString sDocTitle;
									sDocTitle.Format(_T("%s - [%s]"), sCaption, msg->getName());
									pDocument->SetTitle(sDocTitle);

									wc.Restore();

									break;
								}
							}	// if (posView)
						}	// if (nDocCounter == (nNumOfReportsOpen + 1) || nNumOfReportsOpen == 0)
						nDocCounter++;

					}	// while (posDOC != NULL)

					break;
				}	// if (pTemplate && sDocName.Compare(sResStr) == 0)
			}	// while(pos != NULL)
		} // if (sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
		*ret = 1;
	}	// if (bFound)
	else
	{
		*ret = 0;
	}
}


// Use this function, when calling from inside another Suite/User module; 060619 p�d
void DLL_BUILD OpenSuiteEx(_user_msg *msg, CWnd *wnd, vecINDEX_TABLE &vecIndex, int *ret)
{
	runOpenSuiteEx(msg, wnd, vecIndex, ret, TEXT_FILE);
}
